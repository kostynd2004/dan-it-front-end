import React, { useEffect } from "react";
import "./styles/App.scss";
import Header from "./components/Header/Header.jsx";
import Footer from "./components/Footer/Footer.jsx";
import AppRoutes from "./routes/AppRoutes.jsx";
import { useDispatch, useSelector } from "react-redux";
import { goodsOperations } from "../redux/features/goods/index.js";
import { cartOperations } from "../redux/features/cart";
import { favouritesOperations } from "../redux/features/favourites";

export default function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(goodsOperations.getGoods());
        dispatch(cartOperations.getCart());
        dispatch(favouritesOperations.getFavourites());
    }, []);

    return (
        <>
            <Header/>
            <div className="wrapper main">
                <AppRoutes/>
            </div>
            <Footer/>
        </>
    );
}
