import React from "react";
import PropTypes from "prop-types";
import Button from "../../../../components/Button/Button.jsx";
import {
    emptyStarIcon,
    starIcon
} from "../../../../utilities/Icons/iconDetails.js";
import { useDispatch } from "react-redux";
import { favouritesOperations } from "../../../../../redux/features/favourites";
import "./ExtendedProductCard.scss";

ExtendedProductCard.propTypes = {
    chosenItem: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.shape({
            UAH: PropTypes.shape({
                value: PropTypes.string,
                symbol: PropTypes.string,
                oldPrice: PropTypes.string
            })
        }),
        image: PropTypes.string,
        characteristic: PropTypes.string,
        colors: PropTypes.arrayOf(PropTypes.shape({
            title: PropTypes.string,
            rgb: PropTypes.string,
            hex: PropTypes.string
        }))
    }),
    isFavourite: PropTypes.bool
};


/**
 * @todo
 * create currency state in Redux
 * */

function ExtendedProductCard( props ) {
    const {chosenItem, isFavourite} = props;
    const {title, _id, image, characteristic, colors, price} = chosenItem;
    const dispatch = useDispatch();

    // const chosenCurrencyValue = price[reduxCurrency].value;
    // const chosenCurrencySymbol = price[reduxCurrency].symbol;
    // const chosenCurrencyOldPrice = price[reduxCurrency].oldPrice

    let colorList;

    if ( colors ) {
        colorList = colors.map(( color, index ) => {
            const style = {
                backgroundColor: color.rgb
            };
            return (
                <li key={ index } className="product-colors__item">
                    Цвет: <span>{ color.title }</span>
                    <a href="#">
                        <span
                            className="product-colors__color"
                            style={ style }/>

                    </a>
                </li>
            );
        });
    }


    return (
        <div className="product-about">
            <div className="product-about__left">
                <picture className="product-about__picture">
                    <img src={ `../${ image }` } alt={ title + "image" }/>
                </picture>

                <div className="product-about__characteristic">
                    <p>{ characteristic }</p>
                </div>
            </div>
            <div className="product-about__right">
                <div className="product-about__colors product-colors">
                    <ul className="product-colors__list">
                        { colorList }
                    </ul>
                </div>
                <div className="product-trade">
                    <p className="product-trade__price">
                                 <span
                                     className="product-trade__price-value">Цена: { price.UAH.value }</span>
                        <span
                            className="product-trade__price-currency">{ price.UAH.symbol }</span>
                        {/*<span */ }
                        {/*    className="product-trade__price--old">{chosenCurrencyOldPrice} <small>{ chosenCurrencySymbol }</small>   </span>*/ }
                        {/*<span*/ }
                        {/*     className="product-trade__price-value">{ chosenCurrencyValue }</span>*/ }
                        {/*<span*/ }
                        {/*    className="product-trade__price-currency">{ chosenCurrencySymbol }</span>*/ }
                    </p>

                    <div className="product-trade__actions">
                        <Button text="Купить"
                                className="product-trade__btn product-trade__buy-btn"/>
                        <Button type="button"
                                onClick={ () => {
                                    toggleFavourite(_id, dispatch, isFavourite);
                                } }
                                className="product-trade__favourite-btn favourite-btn"
                                children={ isFavourite ?
                                    starIcon : emptyStarIcon }/>
                    </div>
                </div>
            </div>
        </div>

    );
}

function toggleFavourite( id, dispatch, isFavourite ) {
    isFavourite
        ? dispatch(favouritesOperations.removeFromFavourites(id))
        : dispatch(favouritesOperations.addToFavourites(id));
}

export default ExtendedProductCard;