import React from "react";
import {Route, Switch} from "react-router-dom";
import Home from "../pages/Home/Home.jsx";
import Cart from "../pages/Cart/Cart.jsx";
import Favourites from "../pages/Favourites/Favourites.jsx";
import GoodsItem from "../pages/GoodsItem/GoodsItem.jsx";

export default function AppRoutes () {
    return (
       <Switch>
           <Route
               exact path="/"
               render={() => <Home/>}/>
           <Route
               path="/goods/:id"
               render={() => <GoodsItem/>}/>
           <Route
               path="/cart"
               render={() => <Cart/> }/>
           <Route
               path="/favourites"
                render={() => <Favourites />}/>
       </Switch>
    );
}
