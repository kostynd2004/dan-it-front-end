import Icons from "./Icons.jsx";

export const cartHeaderIcon = <Icons
    name="cart"
    className="header-actions__cart-icon"/>;

export const cartGoodsIcon = <Icons
    name="cart"
    className="goods-tile__cart-icon"
    fillColor="#00a046"/>;

export const userIcon = <Icons
    name="user"
    className="header-actions__authorization-icon"
    fillColor="#FFFFFF"/>;

export const starIcon = <Icons
    name="star"
    className="header-actions__favourites-icon"
    fillColor="#FFD700"/>;

export const emptyStarIcon = <Icons
    name="emptyStar"
    fillColor="#FFD700"
    className="header-actions__favourites-icon header-actions__favourites-icon--empty"/>;

export const cartChecked = <Icons
    name="cartChecked"
    className="goods-tile__cart-icon goods-tile__cart-icon--checked"
    fillColor="#00a046"/>;

export const plusIcon = <Icons
    name="plus"
    className="cart-counter__btn-icon plus-btn-icon"
    fillColor="#585F73"/>;

export const minusIcon = <Icons
    name="minus"
    className="cart-counter__btn-icon minus-btn-icon"
    fillColor="#585F73"/>;

export const disabledMinusIcon = <Icons
    name="minus"
    className="cart-counter__btn-icon minus-btn-icon--disabled"
    fillColor="#d2d2d2"/>

    export const disabledPlusIcon = <Icons
    name="plus"
    className="cart-counter__btn-icon plus-btn-icon--disabled"
    fillColor="#d2d2d2"/>
