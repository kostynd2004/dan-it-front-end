import {uniqueKey} from "../../utilities/numberGenerators/numberGenerators.js";

export const modalParameters = [
    {
        id: 0,
        name: "delete",
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it won’t be possible to undo" +
            " this action. Are you sure you want to delete it?",
        closeButton: true,
        actions: [
            {
                key: uniqueKey(),
                text: "OK",
                backgroundColor: "#B3372BFF",
                className: "modal-body__btn modal__submit-btn"
            }, {
                key: uniqueKey(),
                text: "Cancel",
                backgroundColor: "#B3372BFF",
                className: "modal-body__btn modal__cancel-btn"
            }
        ],
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--red",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--red",
            text: "modal-body__text",
            buttons: "modal-body__buttons"
        }
    },
    {
        id: 1,
        name: "cart",
        header: "Вы хотите добавить этот товар в корзину?",
        text: "Этот товар будет перемещен в Вашу корзину",
        closeButton: true,
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--teal",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--teal",
            text: "modal-body__text",
            buttons: "modal-body__buttons"
        }
    },
    {
        id: 2,
        name: "deleteFromCart",
        header: "Вы хотите удалить этот товар из корзины??",
        text: "Этот товар будет удалён из Вашей корзины",
        closeButton: true,
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--teal",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--teal",
            text: "modal-body__text",
            buttons: "modal-body__buttons"
        }

    }
];


let modalIDs = {};

modalParameters.forEach(item => {
    modalIDs[item.name] = item.id;
});

export {modalIDs};
