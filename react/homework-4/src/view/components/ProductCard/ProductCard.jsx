import React, { useEffect, useState } from "react";
import Button from "../Button/Button.jsx";
import { modalIDs } from "../Modal/modalParameters.js";
import {
    starIcon,
    emptyStarIcon,
    cartGoodsIcon,
    cartChecked
} from "../../utilities/Icons/iconDetails.js";
import "./ProductCard.scss";
import PropTypes from "prop-types";
import Modal, { generateModalData } from "../Modal/Modal.jsx";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { cartOperations } from "../../../redux/features/cart";
import {
    favouritesOperations
} from "../../../redux/features/favourites";
import {
    isModalOpenOperations,
    isModalOpenSelectors,
} from "../../../redux/features/isModalOpen";
import { uniqueKey } from "../../utilities/numberGenerators/numberGenerators.js";


ProductCard.propTypes = {
    card: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.object,
        image: PropTypes.string,
        _id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        color: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)])
    }),
    isOnCart: PropTypes.bool.isRequired,
    isFavourite: PropTypes.bool,
    classNames: PropTypes.shape({
        listItem: PropTypes.string
    }),
};


export default function ProductCard( props ) {
    const isOpenModal = useSelector(isModalOpenSelectors.isModalOpen);
    const dispatch = useDispatch();
    const history = useHistory();
    const [openModalID, setOpenModalID] = useState(null);
    const {card, isOnCart, isFavourite} = props;

    useEffect(() => {
        if ( !isOpenModal ) {
            setOpenModalID(null);
        }
    }, [openModalID, isOpenModal]);

    if ( !card ) return null;

    const {title, price, image, _id} = card;
    let chosenModalData;

    if ( openModalID ) {
        chosenModalData = generateModalData(openModalID);
    }


    return (
        <li className={ props.classNames.listItem }>
            <div className="goods-tile">
                <div className="goods-tile__inner">
                    <Link to={ `/goods/${_id}` } className="goods-tile__picture">
                        <img src={ image } alt={ title }/>
                    </Link>
                    <div className="goods-tile__actions">
                        <Button type="button"
                                onClick={ () => {
                                    toggleFavourite(_id, dispatch, isFavourite);
                                } }
                                className="goods-tile__favourite-btn favourite-btn"
                                children={ isFavourite ?
                                    starIcon : emptyStarIcon }/>
                    </div>
                    <div className="goods-tile__colors">
                    </div>
                    <Link to={ `/goods/${_id}` } className="goods-tile__title">{ title }</Link>
                    <div className="goods-tile__prices">
                        <p className="goods-tile__price">
                                <span
                                     className="goods-tile__price-value">{ price.UAH.value }</span>
                            <span
                                className="goods-tile__price-currency">{price.UAH.symbol}</span>
                        </p>
                        <Button
                            className="goods-tile__cart-button"
                            children={ isOnCart ?
                                cartChecked : cartGoodsIcon }
                            onClick={ ( e ) => {
                                cartButtonBehaviour(history, isOnCart, dispatch, setOpenModalID);
                            } }/>
                    </div>
                </div>
            </div>
            { openModalID
            &&
            <Modal { ...chosenModalData }
                   actions={ declareCartModalActionButtons(_id, dispatch) }/> }
        </li>
    );
}

function cartButtonBehaviour( history, isOnCart, dispatch, setOpenModalID ) {
    if ( isOnCart ) {
        history.push("/cart");
        return;
    }

    dispatch(isModalOpenOperations.openModal());
    setOpenModalID(modalIDs.cart);
}

function toggleFavourite( id, dispatch, isFavourite ) {
    isFavourite
        ? dispatch(favouritesOperations.removeFromFavourites(id))
        : dispatch(favouritesOperations.addToFavourites(id));
}

function declareCartModalActionButtons( productID, dispatch ) {
    return [
        <Button
            key={ uniqueKey() }
            text="OK"
            backgroundColor="#23A77BFF"
            className="modal-body__btn modal__submit-btn"
            onClick={ () => dispatch(cartOperations.addToCart(productID, 1)) }
        />,
        <Button
            key={ uniqueKey() }
            text="Отменить"
            backgroundColor="#23A77BFF"
            className="modal-body__btn modal__cancel-btn"
        />
    ];
}
