import actions from "./actions.js";
import {
    deleteData,
    getAllData,
    patchData,
    postData
} from "../../../API/serverAPI.js";

const addToCart = ( id, quantity ) => async ( dispatch ) => {
    await postData("cart", {_id: id, quantity})
    dispatch(actions.addToCart(id, quantity));
};

const setAmount = (id, quantity) => async (dispatch) => {
    await patchData("cart", id, {quantity: quantity});
    const updatedCart = await getAllData("cart")
    dispatch(actions.setAmount(updatedCart));
}

const increaseAmount = ( id ) => async ( dispatch, getState ) => {
    const cart = getState().cart;
    const chosenItem = cart.find(object => object._id === id);
    await patchData("cart", id, {quantity: chosenItem.quantity +=1});
    const updatedCart = await getAllData("cart")
    dispatch(actions.increaseAmount(updatedCart));
};

const decreaseAmount = ( id ) => async ( dispatch, getState ) => {
    const cart = getState().cart;
    const chosenItem = cart.find(object => object._id === id);
    let {quantity} = chosenItem;
    quantity -= 1;
    if ( quantity === 0 ){
        await deleteData("cart", id)
        return
    }
    await patchData("cart", id, {quantity});
    const updatedCart = await getAllData("cart")
    dispatch(actions.decreaseAmount(updatedCart));
};

const removeFromCart = ( id) => async ( dispatch ) => {
    await deleteData("cart", id);
    dispatch(actions.removeFromCart(id));
};

const getCart = () => async ( dispatch ) => {
    const data = await getAllData("cart");
    dispatch(actions.getCart(data));
};

export default {
    setAmount,
    addToCart,
    increaseAmount,
    decreaseAmount,
    removeFromCart,
    getCart,
};