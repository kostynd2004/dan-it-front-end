const ADDED_TO_CART = "cart/addedToCart";
const REMOVED_FROM_CART = "cart/removedFromCart";
const SET_CART = "cart/setCart";
const SET_AMOUNT = "cart/setAmount"
const INCREASED_AMOUNT = "cart/increasedAmount"
const DECREASED_AMOUNT = "cart/decreasedAmount";


export default {
    ADDED_TO_CART,
    SET_AMOUNT,
    DECREASED_AMOUNT,
    INCREASED_AMOUNT,
    REMOVED_FROM_CART,
    SET_CART,
}