const ADDED_TO_FAVOURITES = "favourites/addedToFavourites";
const REMOVED_FROM_FAVOURITES = "favourites/removedFromFavourites";
const SET_FAVOURITES = "favourites/setFavourites";

export default {
    ADDED_TO_FAVOURITES,
    REMOVED_FROM_FAVOURITES,
    SET_FAVOURITES,
}