import types from "./types.js";

const openModal = () => ({
    type: types.OPENED,
})

const closeModal = () => ({
    type: types.CLOSED,
})


export default {
    openModal,
    closeModal
}