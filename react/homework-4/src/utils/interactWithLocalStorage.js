export function putDataInLocalStorage(keyName, data) {
    if (!data) return null;

    localStorage.setItem(keyName, JSON.stringify(data));
}

export function getDataFromLocalStorage (keyName) {
    return JSON.parse(localStorage.getItem(keyName.toString()));
}