import React from "react";
import ReactDOM from "react-dom";
import "./view/styles/index.scss";
import App from "./view/App.js";
import reportWebVitals from "./reportWebVitals";
import ErrorBoundary from "./view/components/ErrorBoundary/ErrorBoundary.jsx";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store/store.js";

ReactDOM.render(
    <BrowserRouter>
        <ErrorBoundary>
            <Provider store={store}>
                <App/>
            </Provider>
        </ErrorBoundary>
    </BrowserRouter>,
    document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
