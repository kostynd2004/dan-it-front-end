const headers = {
    "Content-Type": "application/json;charset=utf-8"
};

/**
 * Function gets all the data in the chosen database collection
 *
 * @async
 * @param {string} collection - the name of the database collection where
 * data is stored. Read server documentation (README) for more information
 *
 *
 * @returns {array} of items that are stored in this collection
 * */
export async function getAllData( collection ) {
    let result;
    try {
        result = await fetch(`/api/${ collection }`);
    } catch (err) {
        throw new Error(err.message);
    }

    if ( !result.ok ){
        throw new Error(`Error, could not get data from "${collection}" response status - ${result.status} ${result.statusText}`)
    }

    return result.json();
}

/**
 * Function gets the object, which has the same id as it was provided,  from the
 * chosen database collection
 *
 * @async
 * @param {string} collection - the name of the database collection where
 * data is stored. Read server documentation (README) for more information
 *
 * @param {string} id - id of the item you want to receive
 *
 * @returns {object} of the item that has provided id
 * */
export async function getDataByID( collection, id ) {
    let result;
    try {
        result = await fetch(`/api/${ collection }/${ id }`);
    } catch (err) {
        throw new Error(err.message);
    }

    if ( !result.ok ){
        throw new Error(`Error, could not get data from "${collection}" response status - ${result.status} ${result.statusText}`)
    }

    return result.json();
}


/**
 * The function posts an object to a specific database collection
 *
 * @async
 * @param {string} collection - the name of the database collection where
 * data is stored. Read server documentation (README) for more information
 *
 * @param {object} data - the data that will be post in the database
 *
 * @returns {object} with an _id field, in case it wasn't mentioned, and the
 * data that was passed as an argument
 * */
export async function postData( collection, data ) {
    let result;
    try {
        result = await fetch(`/api/${ collection }`, {
            method: "POST",
            headers: headers,
            body: JSON.stringify(data)
        });
    } catch (err) {
        throw new Error(err.message);
    }

    if ( !result.ok ){
        throw new Error(`Error, could not post data to "${collection}" response status - ${result.status} ${result.statusText}`)
    }

    return result.json();
}

/**
 * Function that replaces chosen object data in database collection with the
 * one that was passed as an argument
 *
 * @async
 *
 * @param {string} collection - the name of the database collection where
 * data is stored. Read server documentation (README) for more information
 *
 * @param {string} id - id of the item you want to replace
 *
 * @param {object} data - the data you want to put instead of the previous one
 *
 * @returns {object} with the same _id field as well as fields that were
 * passed to data argument
 * */
export async function putData (collection, id, data) {
    let result;
    try {
        result = await fetch(`/api/${collection}/${id}`, {
            method: "PUT",
            headers: headers,
            body: JSON.stringify(data)
        })
    }catch (err){
        throw new Error(err.message)
    }

    if ( !result.ok ){
        throw new Error(`Error, could not put data to "${collection}" response status - ${result.status} ${result.statusText}`)
    }

    return result.json()
}

/**
 * Function that updates particular data in specific database collection.
 * This function do not overwrite the object, it only changes or adds fields
 *
 * @async
 * @param {string} collection - the name of the database collection where
 * data is stored. Read server documentation (README) for more information
 *
 * @param {string} id - id of the item you want to update
 *
 * @param {object} data - the data you want to change or add to the existing one
 *
 * @returns {object} that was updated
 * */
export async function patchData (collection, id, data) {
    let result;
    try {
        result = await fetch(`/api/${collection}/${id}`,{
            method: "PATCH",
            headers: headers,
            body: JSON.stringify(data)
        })
    } catch (err) {
        throw new Error(err)
    }

    if ( !result.ok ){
        throw new Error(`Error, could not patch data to "${collection}" response status - ${result.status} ${result.statusText}`)
    }

    return result.json()

}

/**
 * This function deletes an object from a specific database collection
 *
 * @async
 * @param {string} collection - the name of the database collection where
 * data is stored. Read server documentation (README) for more information
 *
 * @param {string} id - id of the item you want to delete
 * */
export async function deleteData( collection, id ) {
    let result;
    try {
        result = await fetch(`/api/${ collection }/${ id }`, {
            method: "DELETE",
            headers: headers,
        });
    } catch (err) {
        throw new Error(err.message);
    }

    if ( !result.ok ){
        throw new Error(`Error, could not delete data from "${collection}" response status - ${result.status} ${result.statusText}`)
    }
}

