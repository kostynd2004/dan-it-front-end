import types from "./types.js";

const reducer = ( state = [], action ) => {
    const {type, payload} = action;
    const {
        ADDED_TO_CART,
        REMOVED_FROM_CART,
        SET_CART,
        SET_AMOUNT,
        INCREASED_AMOUNT,
        DECREASED_AMOUNT,
        REMOVED_ALL
    } = types;

    if ( type === ADDED_TO_CART ) {
        return [...state, payload];
    } else if ( type === SET_AMOUNT ) {
        return payload;
    } else if ( type === INCREASED_AMOUNT ) {
        return payload;
    } else if ( type === DECREASED_AMOUNT ) {
        return payload;
    } else if ( type === SET_CART ) {
        return payload;
    } else if ( type === REMOVED_FROM_CART ) {
        return state.filter(object => object._id !== payload);
    }else if ( type === REMOVED_ALL ) {
        return []
    }  else {
        return state;
    }
};

export default {
    cart: reducer
};