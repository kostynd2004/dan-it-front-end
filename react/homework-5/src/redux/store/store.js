import { applyMiddleware, combineReducers, createStore } from "redux";
import goodsReducers from "../features/goods/index.js"
import favouritesReducers from "../features/favourites/index.js"
import cartReducers from "../features/cart/index.js"
import isModalOpenReducers from "../features/isModalOpen/index.js"

import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
    ...goodsReducers,
    ...favouritesReducers,
    ...cartReducers,
    ...isModalOpenReducers
})

const composeEnhancers = composeWithDevTools({
    trace: true
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export default store;

