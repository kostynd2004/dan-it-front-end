import React from "react";
import GoodsList from "./GoodsList/GoodsList.jsx";
import "./Goods.scss";

export default function Goods() {

    const classNames={
        cardList: "goods__list",
        listItem: "goods__cell"
    }

    return (
        <section className="goods">
            <GoodsList classNames={classNames} />
        </section>
    );
}



