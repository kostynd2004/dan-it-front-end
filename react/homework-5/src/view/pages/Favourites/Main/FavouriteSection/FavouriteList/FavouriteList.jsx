import React from "react";
import PropTypes from "prop-types";
import ProductCard from "../../../../../components/ProductCard/ProductCard.jsx";
import { useSelector } from "react-redux";
import { cartSelectors } from "../../../../../../redux/features/cart";
import { goodsSelectors } from "../../../../../../redux/features/goods";
import { favouritesSelectors } from "../../../../../../redux/features/favourites";

FavouriteList.propTypes = {
    classNames: PropTypes.shape({
        cardList: PropTypes.string,
        listItem: PropTypes.string
    })
};

export default function FavouriteList(props) {
    const cart = useSelector(cartSelectors.cart);
    const goods = useSelector(goodsSelectors.goods);
    const favourites = useSelector(favouritesSelectors.favourites);

    if ( !favourites ){
        return <p className="loading"> Загрузка ...</p>
    }

    if (!favourites.length) {
        return <p className="alert no-goods-alert">Товаров не добавлено</p>;
    }

    const favouriteCards = goods.filter(card => {
        return favourites.some(object => card._id === object._id)

    })

    const list = favouriteCards.map(card => {
        const isOnCart = cart.some(object => object._id === card._id);

        return <ProductCard
            key={card._id}
            card={card}
            isOnCart={isOnCart}
            isFavourite={true}
            {...props}/>
    })

    return (
        <ul className={props.classNames.cardList}>
            {list}
        </ul>
    );
}

