import React from "react";
import PropTypes from "prop-types";

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
};

Button.defaultProps = {
    className: "btn",
    text: "",
    backgroundColor: "",
    children: ""
};


function Button(props) {
    let {
        backgroundColor,
        className,
        onClick,
        text,
        children,
        ...restProps
    } = props;

    const styles = {
        backgroundColor: backgroundColor
    };

    return (
        <button {...restProps}
                className={className}
                onClick={(e) => {
                    return onClick ? onClick(e) : null;
                }}
                style={styles}>
            {text}
            {children}
        </button>
    );
}


export default Button;