import React, { useEffect } from "react";
import "./styles/App.scss";
import Header from "./components/Header/Header.jsx";
import Footer from "./components/Footer/Footer.jsx";
import AppRoutes from "./routes/AppRoutes.jsx";
import { useDispatch } from "react-redux";
import { goodsOperations } from "../redux/features/goods/index.js";
import { cartOperations } from "../redux/features/cart";
import { favouritesOperations } from "../redux/features/favourites";

export default function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(goodsOperations.getGoods());
        dispatch(cartOperations.getCart());
        dispatch(favouritesOperations.getFavourites());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <Header/>
            <div className="wrapper main">
                <AppRoutes/>
            </div>
            <Footer/>
        </>
    );
}
