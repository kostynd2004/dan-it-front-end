const {replaceItem, updateItem, deleteByID, getAll, getByID} = require("./../utils/utils.js")
const ObjectID = require("mongodb").ObjectID;

module.exports = function ( app, collection ) {
    const CART_PATH = "/api/cart"
    app.get(`${CART_PATH}`, ( req, res ) => {
        getAll(collection, res);
    });

    app.get(`${CART_PATH}/:id`, ( req, res ) => {
        const id = req.params.id;

        getByID(id, collection, res)
    });

    app.post(`${CART_PATH}`, ( req, res ) => {
        const {body} = req;
        const item = {
            _id: new ObjectID(body._id),
            quantity: body.quantity
        }

        collection.insertOne(item, ( err, result ) => {
            if ( err ) {
                res.send({"error": "An error has been occurred"});
                return console.log("cart POST", err);
            }

            res.send(result.ops);
        });
    });

    app.put(`${CART_PATH}/:id`, (req, res) => {
        const id = req.params.id;
        const item = req.body

        replaceItem(id, collection, item, res)
    })

    app.patch(`${CART_PATH}/:id`, (req, res) => {
        const id = req.params.id;
        const item = req.body;

        updateItem(id, collection, item, res)
    })

    app.delete(`${CART_PATH}/:id`, ( req, res ) => {
        const id = req.params.id;

        deleteByID(id, collection, res)
    });


};