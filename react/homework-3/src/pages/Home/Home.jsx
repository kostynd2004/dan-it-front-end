import React, {useContext} from "react";
import PropTypes from "prop-types"
import Main from "./Main/Main.jsx";
import Aside from "../../components/Aside/Aside.jsx";
import {addItemToCart} from "../../App.js";
import {CartContext, FavouritesContext, GoodsContext} from "../../context.js";
import {toggleModalClosing} from "../../App.js";
import {toggleFavourite} from "../../App.js";

Home.propTypes = {
    setCart: PropTypes.func.isRequired,
    setFavourites: PropTypes.func.isRequired,
    openModalID: PropTypes.string,
    setModalID: PropTypes.func.isRequired
}

export default function Home(props) {
    const {
        openModalID,
        setModalID,
        setCart,
        setFavourites
    } = props;
    const goods = useContext(GoodsContext)
    const cart = useContext(CartContext)
    const favourites = useContext(FavouritesContext)

    if (goods.length === 0) {
        return <p className="loading">Loading ...</p>;
    }

    return (
        <React.Fragment>
            <Aside/>
            <Main
                addItemToCart={(id) => setCart(addItemToCart(id,goods, cart))}
                toggleModalClosing={(e) => setModalID(toggleModalClosing(e, openModalID))}
                toggleFavourite={(id) => setFavourites(toggleFavourite(id, goods, favourites))}/>
        </React.Fragment>
    );
}
