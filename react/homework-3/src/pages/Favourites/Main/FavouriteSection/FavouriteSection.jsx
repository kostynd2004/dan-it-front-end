import React from "react";
import PropTypes from "prop-types";
import GoodsList from "../../../Home/Main/Goods/GoodsList/GoodsList.jsx";
import FavouriteList from "./FavouriteList/FavouriteList.jsx";

FavouriteSection.propTypes = {
    addItemToCart:PropTypes.func.isRequired,
    toggleModalClosing:PropTypes.func.isRequired,
    toggleFavourite:PropTypes.func.isRequired
};

function FavouriteSection(props) {

    const classNames={
        cardList: "favourites__list",
        listItem: "favourites__cell"
    }

    return (
        <section className="favourites">
            <FavouriteList classNames={classNames} {...props}/>
        </section>
    );
}

export default FavouriteSection;