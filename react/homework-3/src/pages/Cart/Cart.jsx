import React, {useContext} from "react";
import PropTypes from "prop-types";
import Main from "./Main/Main.jsx";
import {addItemToCart, removeItemFromCart, removeAllSimilarItemsFromCart} from "../../App.js";
import {CartContext, FavouritesContext, GoodsContext} from "../../context.js";

Cart.propTypes = {
    setCart: PropTypes.func.isRequired,
}


export default function Cart (props){
    const {
        setCart
    } = props;
    const goods = useContext(GoodsContext)
    const cart = useContext(CartContext)

    return (
        <>
        <div></div>
       <Main
           {...props}
           addItemToCart={(id) => setCart(addItemToCart(id,goods, cart))}
           removeItemFromCart={(id) => setCart(removeItemFromCart(id, cart))}
           removeAllSimilarItemsFromCart={(id) => setCart(removeAllSimilarItemsFromCart(id, cart))}
       />
        </>
    );
}
