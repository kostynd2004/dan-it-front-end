import React, {useContext, useState} from "react";
import {CartContext} from "../../../../../context.js";
import PropTypes from "prop-types";
import Button from "../../../../../components/Common/Button/Button.jsx";
import {getChosenModal, toggleCartModal} from "../../../../../App.js";
import "./CartItem.scss";
import {plusIcon, minusIcon, disabledMinusIcon} from "../../../../../utilities/Icons/iconDetails.js";
import Modal from "../../../../../components/Common/Modal/Modal.jsx";
import {modalIDs} from "../../../../../components/Common/Modal/ModalParameters.js";

CartItem.propTypes = {
    addItemToCart: PropTypes.func.isRequired,
    removeItemFromCart: PropTypes.func.isRequired,
    removeAllSimilarItemsFromCart: PropTypes.func.isRequired,
    quantity: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    card: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.string,
        image: PropTypes.string,
        id: PropTypes.string,
        color: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)])
    }),
    cart: PropTypes.arrayOf(PropTypes.string),
};

function CartItem(props) {
    let {card, quantity} = props;
    const {title, price, image, id} = card;
    const [openModalID, setOpenModalID] = useState("");
    const cart = useContext(CartContext)
    quantity = Number(quantity);

    const chosenModal = getChosenModal(openModalID, setOpenModalID);

    return (
        <div className="cart-tile">
            <div className="cart-tile__inner">
                <a className="cart-tile__picture-link">
                    <picture className="cart-tile__picture">
                        <img src={image} alt={title}/>
                    </picture>
                </a>
                <div className="cart-tile__details">
                    <p className="cart-tile__title">{title}</p>
                    <div className="cart-tile__counter cart-counter">
                        <Button
                            disabled={quantity === 1}
                            children={quantity === 1 ? disabledMinusIcon : minusIcon}
                            onClick={() => props.removeItemFromCart(id)}
                            className="cart-counter__btn minus-btn"/>
                        <input className="cart-counter__quantity" value={quantity}/>
                        <Button
                            onClick={() => props.addItemToCart(id)}
                            children={plusIcon}
                            className="cart-counter__btn plus-btn"/>
                    </div>
                </div>
                <div className="cart-tile__prices">
                    <p className="cart-tile__price">
                        <span className="cart-tile__price-value">{price.toString()}</span>
                        <span className="cart-tile__price-currency">₴</span>
                    </p>
                    <Button text="Купить" className="cart-tile__buy-btn"/>
                </div>
                <Button
                        data-modal-id={modalIDs.deleteFromCart}
                        data-card-id={id}
                        text="X"
                        className="cart-tile__cross-btn"
                        onClick={(e) => handleCrossBtnClick(e,id, cart , props.removeAllSimilarItemsFromCart,setOpenModalID)}/>
            </div>
            {chosenModal
            &&
            <Modal {...chosenModal}/>}
        </div>
    );
}

function handleCrossBtnClick(e,id,cart, removeAllSimilarItemsFromCart, setOpenModalID) {
    toggleCartModal(e, cart,removeAllSimilarItemsFromCart, setOpenModalID )
}

export default CartItem;