import React, {useState, useEffect} from "react";
import "./styles/App.scss";
import Header from "./components/Header/Header.jsx";
import Footer from "./components/Footer/Footer.jsx";
import Modal from "./components/Common/Modal/Modal";
import AppRoutes from "./routes/AppRoutes.jsx";
import {CartContext, GoodsContext, FavouritesContext} from "./context.js";
import {createDeepCopy} from "./utilities/createDeepCopy/createDeepCopy.js";
import {ModalParameters} from "./components/Common/Modal/ModalParameters.js";
import Button from "./components/Common/Button/Button.jsx";
import {findDataSetName} from "./utilities/findDataSetName/findDataSetName.js";

export default function App() {
    const [openModalID, setModalID] = useState("");
    const [goods, setGoods] = useState([]);
    const [cart, setCart] = useState([]);
    const [favourites, setFavourites] = useState([]);

    const chosenModal = getChosenModal(openModalID);


    useEffect(() => {
        const goods = JSON.parse(localStorage.getItem("goods")) || [];
        const cart = JSON.parse(localStorage.getItem("cart")) || [];
        const favourites = JSON.parse(localStorage.getItem("favourites")) || [];

        if (cart.length > 0) setCart(cart);
        if (favourites.length > 0) setFavourites(favourites);
        if (goods.length > 0) return setGoods(goods);

        fetch("./goods.json")
            .then(response => response.json())
            .then(data => {
                    if (!data) return null;
                    setGoods(data.allGoods);
                    putDataInLocalStorage("goods", data.allGoods);
                    setCart(data.cart);
                    putDataInLocalStorage("cart", data.cart);
                    setFavourites(data.favourites);
                    putDataInLocalStorage("favourites", data.favourites);
                },
                // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
                // чтобы не перехватывать исключения из ошибок в самих компонентах.
                (error) => console.error(error, error.message));
    }, []);

    return (
        <GoodsContext.Provider value={goods}>
            <CartContext.Provider value={cart}>
                <FavouritesContext.Provider value={favourites}>
                        <Header/>
                        <div className="wrapper main">
                            <AppRoutes
                                setCart={setCart}
                                setFavourites={setFavourites}
                                openModalID={openModalID}
                                setModalID={setModalID}
                            />
                        </div>
                        <Footer/>
                        {chosenModal && <Modal {...chosenModal} />}
                </FavouritesContext.Provider>
            </CartContext.Provider>
        </GoodsContext.Provider>
    );
}

export function putDataInLocalStorage(keyName, data) {
    if (!data) return null;

    localStorage.setItem(keyName, JSON.stringify(data));
}

export function removeItemFromCart(id, cart) {
        const copy = [...cart]
        const index = copy.indexOf(id.toString());
        copy.splice(index, 1);
        putDataInLocalStorage("cart", copy);
        return copy;
}

export function removeAllSimilarItemsFromCart(id, cart){
    const result = cart.filter(item => item !== id);
    putDataInLocalStorage("cart", result);
    return result;
}

export function addItemToCart(id, goods,cart ){
    const chosenItem = goods.find(item => item.id === id);
    putDataInLocalStorage("cart", [...cart, chosenItem.id])
    return[...cart, chosenItem.id];
}

export function toggleFavourite(chosenCardID, goods ,favourites) {
    const newFavourites = toggleGoodsTab(chosenCardID, goods, favourites);
    const newFavouritesIDs = newFavourites.map(item => {
        if (typeof item !== "object") return item
        return item.id
    })
    putDataInLocalStorage("favourites", newFavouritesIDs);
    return newFavouritesIDs;
}


export function toggleModalClosing(event, openModalID) {
    const modalID = findDataSetName(event,
        "modalId",
        "[data-modal-id]");

    if (openModalID === modalID) return "";

    return modalID;
}

export function toggleGoodsTab(chosenCardID, goods, tab) {

    const chosenItem = goods.find(item => item.id.toString() === chosenCardID.toString());
    if (!chosenItem) return null;

    let itemInTab;
    if (tab.length !== 0) {
        itemInTab = tab.find(id => id === chosenCardID);
    }

    if (itemInTab) {
        tab = tab.filter(id => id !== chosenCardID);
    } else {
        tab.push(chosenItem.id);
    }

    return tab;
}

export function getChosenModal(openModalID, setOpenModalID) {

    if (!openModalID) return null;

    /*
    * This method is used to make a deep copy. Because there are
    *  Arrays in there that don't change their place in memory if
    shallow copy is used. For instance : [..spreadOperator],
   slice(), Object.assign() etc
    * */

    const ModalParametersCopy = createDeepCopy(ModalParameters);


    let chosenModal = ModalParametersCopy.find(modal => {
        return modal.modalID.toString() === openModalID.toString();
    });

    if (!chosenModal) return null;

    chosenModal.actions = chosenModal.actions.map(btn => {
        return <Button {...btn}/>;
    });

    chosenModal.toggleModalClosing = (e) => setOpenModalID(toggleModalClosing(e, openModalID));

    return chosenModal;
}

export function toggleCartModal(event, cart, submitButtonHandler, setOpenModalID) {
    const chosenModalID = findDataSetName(event,
        "modalId",
        "[data-modal-id]");
    const chosenCardId = findDataSetName(event,
        "cardId",
        "[data-card-id]");

    const chosenModal = ModalParameters.find(modal =>
        modal.modalID.toString() === chosenModalID.toString());

    if (!chosenModal) return null;

    const submitBtn = chosenModal.actions.find(btn => btn.name === "submit-btn");

    submitBtn.onClick = (e) => {
        submitButtonHandler(chosenCardId);
    };

    setOpenModalID(chosenModalID);
}

