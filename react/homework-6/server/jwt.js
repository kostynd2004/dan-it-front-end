const jwt = require("jsonwebtoken");
const jwtConfig = require("./config/jwt.js");

const checkToken = (req, res, next) => {
    const {headers} = req;
    // Express headers are auto converted to lowercase
    let token = headers["x-access-token"] || headers["authorization"];

    if ( token.startsWith("Bearer ")){
        //Remove Bearer from string
        token = token.slice(7, token.length);
        if ( token ) {
            jwt.verify(token, jwtConfig.secret, (err, decoded) => {
                if ( err ) {
                    return res.json({
                        success: false,
                        message: "Token is not valid"
                    });
                }

                req.decoded = decoded;
                next()
                return;
            })
        }

        return res.json({
            success: false,
            message: "Auth token is not supplied"
        })
    }
}

module.exports = {
    checkToken
}