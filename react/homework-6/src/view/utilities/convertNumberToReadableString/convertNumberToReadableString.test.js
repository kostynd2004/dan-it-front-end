import {convertNumberToReadableString} from "./convertNumberToReadableString";

describe("ConvertNumberToReadableString", () => {
    test('should convert integer to a string', function () {
        expect(convertNumberToReadableString(10000)).toBe("10 000");
    });

    test('should convert floating number to a string', function () {
        expect(convertNumberToReadableString(10000.75)).toBe("10 000.75");
    });

    test('should convert floating number with 0 in the end to a normal string', function () {
        expect(convertNumberToReadableString(10000000.50)).toBe("10 000 000.50")
    });

    test('should convert string with a number to an another string', function () {
        expect(convertNumberToReadableString("10000")).toBe("10 000");
    });

    test('should trim a string with extra spaces', function () {
        expect(convertNumberToReadableString("     12000     ")).toBe("12 000");
    });

    test('should trim extra spaces inside of a string', function () {
        expect(convertNumberToReadableString("   12 000    ")).toBe("12 000")
    });

    test('should throw an exception, because of an object passed as an arg ', function () {
        expect(() => {
            convertNumberToReadableString({})
        }).toThrow()
    });

    test('should throw an exception, because of an array passed as an arg ', function () {
        expect(() => {
            convertNumberToReadableString([])
        }).toThrow()
    });
    test('should throw an exception, because of a null passed as an arg ', function () {
        expect(() => {
            convertNumberToReadableString(null)
        }).toThrow()
    });
    test('should throw an exception, because of an undefined passed as an arg ', function () {
        expect(() => {
            convertNumberToReadableString(undefined)
        }).toThrow()
    });

})