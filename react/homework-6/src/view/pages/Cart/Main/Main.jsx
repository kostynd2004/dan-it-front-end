import React from "react";
import CartSection from "./CartSection/CartSection.jsx";

function Main() {
    return (
        <main data-testid="main-content" className="main-content">
            <h1 className="main-content__heading"> Корзина</h1>
            <CartSection/>
        </main>
    );
}

export default Main;