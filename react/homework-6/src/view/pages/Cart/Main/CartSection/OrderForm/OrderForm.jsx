import React from "react";
import { Formik } from "formik";
import { formValidationSchema } from "./formValidationSchema.js";
import NumberFormat from "react-number-format";
import "./OrderForm.scss";
import { useDispatch } from "react-redux";
import { cartOperations } from "../../../../../../redux/features/cart";
import { isModalOpenOperations } from "../../../../../../redux/features/isModalOpen";

function OrderForm( props ) {
    const dispatch = useDispatch();
    return (
        <Formik
            initialValues={ {
                firstName: "",
                lastName: "",
                age: "",
                address: "",
                email: "",
                mobileNumber: "",
            } }
            validationSchema={ formValidationSchema }
            onSubmit={ ( values, actions ) =>
                handleFormSubmit(values, actions, dispatch) }

        >
            { props => {
                const {
                    errors,
                    touched,
                    isSubmitting,
                    isValid,
                    isValidating,
                    handleSubmit
                } = props;
                return (
                    <form className="order-form"
                          onSubmit={ handleSubmit }>
                        <label htmlFor="firstName"
                               className="order-form__label">
                            Name *
                        </label>
                        <input
                            id="firstName"
                            className="order-form__input"
                            name="firstName"
                            placeholder="Enter your name"
                            type="text"
                            onChange={ props.handleChange }
                            onBlur={ props.handleBlur }
                            value={ props.values.name }
                        />

                        { generateError("firstName", errors, touched) }
                        <label htmlFor="lastName" className="order-form__label">
                            Last name *
                        </label>

                        <input
                            id="lastName"
                            className="order-form__input"
                            name="lastName"
                            placeholder="Enter your last name"
                            type="text"
                            onChange={ props.handleChange }
                            onBlur={ props.handleBlur }
                            value={ props.values.lastName }
                        />
                        { generateError("lastName", errors, touched) }
                        <label htmlFor="age" className="order-form__label">
                            Age *
                        </label>
                        <input
                            id="age"
                            className="order-form__input"
                            name="age"
                            placeholder="Enter your age"
                            type="number"
                            onChange={ props.handleChange }
                            onBlur={ props.handleBlur }
                            value={ props.values.age }
                        />
                        { generateError("age", errors, touched) }
                        <label htmlFor="address" className="order-form__label">
                            Address *
                        </label>
                        <input
                            id="address"
                            className="order-form__input"
                            name="address"
                            placeholder="Enter your address"
                            type="text"
                            onChange={ props.handleChange }
                            onBlur={ props.handleBlur }
                            value={ props.values.address }
                        />

                        { generateError("address", errors, touched) }
                        <label htmlFor="email" className="order-form__label">
                            Email *
                        </label>
                        <input
                            id="email"
                            className="order-form__input"
                            name="email"
                            placeholder="Enter your email"
                            type="email"
                            onChange={ props.handleChange }
                            onBlur={ props.handleBlur }
                            value={ props.values.email }
                        />
                        { generateError("email", errors, touched) }
                        <label htmlFor="mobileNumber"
                               className="order-form__label">
                            Mobile number
                        </label>
                        <NumberFormat
                            className="order-form__input"
                            name="mobileNumber"
                            onChange={ props.handleChange }
                            onBlur={ props.handleBlur }
                            value={ props.values.mobileNumber }
                            format="+380 (##) ### ## ##"
                            allowEmptyFormatting/>
                        { generateError("mobileNumber", errors, touched) }
                        <button

                            className="order-form__submit"
                            type="submit"
                            disabled={ !isValid || isSubmitting || isValidating }
                        >
                            Подтвердить
                        </button>
                    </form>
                );
            }
            }
        </Formik>
    );
}

function generateError( name, errors, touched ) {
    return errors[name] && touched[name] &&
        <p className="order-form__error">{ errors[name] }</p>;
}

function handleFormSubmit( values, actions, dispatch ) {
    dispatch(cartOperations.removeAll());
    dispatch(isModalOpenOperations.closeModal())
}

export default OrderForm;