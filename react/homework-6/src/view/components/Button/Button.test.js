import Button from "./Button";
import {cleanup, screen, render} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("Button", () => {
    afterEach(cleanup);

    const {getByTestId, getByText} = screen;
    const props = {
        backgroundColor: "#ff0000",
        text: "test",
        className: "test-btn",
        children: <p data-testid="button-text" >Test</p>,
        onClick: jest.fn()
    }

    test("Smoke", () => {
        render(<Button/>)
    })

    test("If button renders with a default className", () => {
        render(<Button/>)
        expect(getByTestId("button")).toHaveClass("btn")
    })

    test("if button renders with correct text", () => {
        render(<Button text={props.text}/>);
        expect(getByText(props.text)).toBeInTheDocument();
    })

    test("if button renders with correct className", () => {
        render(<Button className={props.className}/> );
        expect(getByTestId("button")).toHaveClass(props.className);
    })

    test("if button renders with correct backgroundColor", () => {
        render(<Button backgroundColor={props.backgroundColor}/> );
        expect(getByTestId("button")).toHaveStyle(`background-color: ${props.backgroundColor};`);
    })

    test("if button renders with a passed child", () => {
        render(<Button children={props.children}/>)
        expect(getByTestId("button-text")).toBeInTheDocument();
    });

    test("if button click handler works properly", () => {
        render(<Button onClick={props.onClick}/>);
        userEvent.click(getByTestId("button"));
        expect(props.onClick).toHaveBeenCalled()
    })

})


