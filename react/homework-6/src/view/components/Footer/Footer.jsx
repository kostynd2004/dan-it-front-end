import React from "react";
import "./Footer.scss"

export default function Footer (props) {
        return (
            <footer className="footer">
                <p className="footer__copyright">
                    Copyright © 2021  | All Rights Reserved
                </p>
            </footer>
        );
}
