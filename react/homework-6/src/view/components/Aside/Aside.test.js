import React from "react";
import Aside from "./Aside";
import {render} from "@testing-library/react";
import {BrowserRouter} from "react-router-dom";

describe("Aside", () => {
    test("Smoke", () => {
       const {getByTestId} = render(
            <BrowserRouter>
                <Aside/>
            </BrowserRouter>
        );

       expect(getByTestId("aside")).toBeInTheDocument();
    })
})