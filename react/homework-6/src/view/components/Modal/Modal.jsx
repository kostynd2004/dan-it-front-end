import React, {useEffect, useState} from "react";
import {modalParameters} from "./modalParameters.js";
import Button from "../Button/Button.jsx";
import PropTypes from "prop-types";
import "./Modal.scss";
import "../../styles/common.scss";
import {useDispatch, useSelector} from "react-redux";
import {
    isModalOpenOperations,
    isModalOpenSelectors
} from "../../../redux/features/isModalOpen";


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string,
    closeButton: PropTypes.bool.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    actions: PropTypes.arrayOf(PropTypes.element),
    children: PropTypes.element,
    classNames: PropTypes.shape({
        modal: PropTypes.string,
        modalBody: PropTypes.string,
        crossButton: PropTypes.string,
        header: PropTypes.string,
        headerWrapper: PropTypes.string,
        text: PropTypes.string,
        buttons: PropTypes.string
    })
};

Modal.defaultProps = {
    classNames: {
        modal: "modal",
        modalBody: "modal-body",
        crossButton: "cross-btn modal-body__cross-btn",
        header: "modal-body__header",
        headerWrapper: "modal-body__header-wrapper",
        text: "modal-body__text",
        buttons: "modal-body__buttons"
    }
};

export default function Modal(props) {
    const {
        id,
        header,
        text,
        closeButton = false,
        actions,
        classNames,
        children,
        ...restProps
    } = props;
    const dispatch = useDispatch();
    const isModalOpen = useSelector(isModalOpenSelectors.isModalOpen);
    const [isCurrentModalOpen, setCurrentModalOpen] = useState(true);

    useEffect(() => {
        if (!isModalOpen) {
            setCurrentModalOpen(false);
        }
    }, [isModalOpen, isModalOpen]);

    let crossButton = false;
    if (closeButton) {
        crossButton = <Button
            text="X"
            backgroundColor="transparent"
            className={classNames.crossButton}
            onClick={(e) => handleModalClosing(e, dispatch)}/>;
    }


    useEffect(() => {
        document.body.classList.add("lock-screen");
        return () => {
            document.body.classList.remove("lock-screen");
        };
    }, []);


    return isCurrentModalOpen && (
        <div {...restProps} data-modal-id={id} onClick={(e) => handleModalClosing(e, dispatch)}
             className={classNames.modal}>
            <div  className={classNames.modalBody}>
                <div className={classNames.headerWrapper}>
                    {closeButton && crossButton}
                    <h3 className={classNames.header}>{header}</h3>
                </div>
                {children}
                <p className={classNames.text}>{text}</p>
                <div className={classNames.buttons}>
                    {actions}
                </div>
            </div>
        </div>);
}

export function handleModalClosing(e, dispatch) {
    const target = e.target;
    const targetTagName = e.target.tagName.toLowerCase();
    if (target === e.currentTarget || (targetTagName === "button" && !target.closest("form"))) {
        dispatch(isModalOpenOperations.closeModal());
    }
}

export function generateModalData(id) {
    return modalParameters.find(modal =>
        modal.id.toString().trim() === id.toString().trim());
}

