import actions from "./actions";
import types from "./types";
import reducers from "./reducers";
import selectors from "./selectors";
import {isModalOpenOperations} from "./index";

const {
    OPENED,
    CLOSED
} = types;

const {
    openModal,
    closeModal
} = actions;

describe("reducers", () => {
    const reducer = reducers.isModalOpen;
    test("smoke", () => {
        const result = reducer(undefined, {type: "@@INIT"});
        expect(result).toBeFalsy();
    });

    test("if it returns correct state in case OPENED action is passed", () => {
        const action = openModal();
        const result = reducer(undefined, action);
        expect(result).toBeTruthy();
    });

    test("if it returns correct state in case CLOSED action is passed", () => {
        const action = closeModal();
        const result = reducer(undefined, action);
        expect(result).toBeFalsy();
    });
});

describe("selectors", () => {
    const isModalOpen = selectors.isModalOpen;
   test("smoke", () => {
       const state = {
           isModalOpen: true
       }
       const result = isModalOpen(state);
       expect(result).toEqual(state.isModalOpen);
   });
});

describe("actions", () => {
    describe("openModal", () => {
        test("smoke", () => {
            expect(typeof openModal()).toBe("object");
        })

        test("return value if no arguments is passed", () => {
            const output = {
                type: OPENED
            };
            expect(openModal()).toStrictEqual(output);
        })
    });

    describe("closeModal", () => {
        test("smoke", () => {
            expect(typeof closeModal()).toBe("object");
        })

        test("return value if no arguments is passed", () => {
            const output = {
                type: CLOSED
            };
            expect(closeModal()).toStrictEqual(output);
        })
    })
});

describe("operations", () => {
    describe("openModal", () => {
        test("smoke", () => {
           expect(typeof isModalOpenOperations.openModal()).toBe("function");
        });
    });

    describe("closeModal", () => {
        test("smoke", () => {
            expect(typeof isModalOpenOperations.closeModal()).toBe("function");
        });
    });
})