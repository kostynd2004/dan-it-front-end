import types from "./types.js";

const reducer = (state = false, action) => {
    const {type} = action;
    const {OPENED, CLOSED} = types

    if ( type === OPENED ){
        return true
    } else if ( type === CLOSED ){
        return false
    } else{
        return state
    }
}

export default {
    isModalOpen: reducer
}