const ADDED_TO_CART = "cart/addedToCart";
const REMOVED_FROM_CART = "cart/removedFromCart";
const GOT_CART = "cart/gotCart";
const SET_AMOUNT = "cart/setAmount";
const INCREASED_AMOUNT = "cart/increasedAmount";
const DECREASED_AMOUNT = "cart/decreasedAmount";
const REMOVED_ALL = "cart/removedAll"


export default {
    ADDED_TO_CART,
    SET_AMOUNT,
    DECREASED_AMOUNT,
    INCREASED_AMOUNT,
    REMOVED_FROM_CART,
    GOT_CART,
    REMOVED_ALL
}