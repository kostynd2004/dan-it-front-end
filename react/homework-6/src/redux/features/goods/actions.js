import types from "./types.js";

const getData = ( data) => ({
    type: types.GOT_DATA,
    payload: data
})

export default {
    getData,
}