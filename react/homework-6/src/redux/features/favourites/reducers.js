import types from "./types.js";

const reducer = ( state = [], action ) => {
    const {type, payload} = action;
    const {
        ADDED_TO_FAVOURITES,
        SET_FAVOURITES,
        REMOVED_FROM_FAVOURITES
    } = types;

    if ( type === ADDED_TO_FAVOURITES ){
        return [...state, payload]
    } else if (type === REMOVED_FROM_FAVOURITES) {
        return state.filter(object => object._id !== payload);
    } else if (type === SET_FAVOURITES){
        return payload
    }  else {
        return state
    }
};

export default {
    favourites: reducer
}