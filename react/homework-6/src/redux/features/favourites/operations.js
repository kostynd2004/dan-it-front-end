import actions from "./actions.js";

import { deleteData, getAllData, postData } from "../../../API/serverAPI.js";

const addToFavourites = (id) => async (dispatch) => {
    await postData("favourites", { _id: id});
    dispatch(actions.addToFavourites(id))
}

const removeFromFavourites = (id) => async (dispatch ) => {
    await deleteData("favourites", id)
    dispatch(actions.removeFromFavourites(id));
}

const getFavourites = () => async (dispatch) =>{
    const data = await getAllData("favourites")
    dispatch(actions.setFavourites(data))
}

const setFavourites = (data) => (dispatch) => {
    return dispatch(actions.setFavourites(data))
}

export default {
    addToFavourites,
    removeFromFavourites,
    getFavourites,
    setFavourites
}