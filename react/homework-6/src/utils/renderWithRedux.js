import {createStore} from "redux";
import modalReducer from "../redux/features/isModalOpen";
import {render} from "@testing-library/react";
import {Provider} from "react-redux";

const renderWithRedux = (
    component,
    {
        initialState,
        store = createStore(modalReducer.isModalOpen, initialState)
    } = {}
) => {
    return {
        ...render(<Provider store={store}>{component}</Provider>),
        store
    }
}

export default renderWithRedux;