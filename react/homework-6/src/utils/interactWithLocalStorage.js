export function putDataInLocalStorage(keyName, data) {
    if (!data) throw "invalid data"

    localStorage.setItem(keyName, JSON.stringify(data));
}

export function getDataFromLocalStorage (keyName) {
    if (!keyName) throw "invalid key"

    return JSON.parse(localStorage.getItem(keyName.toString()));
}