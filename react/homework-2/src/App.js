import React from "react";
import "./styles/App.scss";
import Header from "./components/Header/Header.jsx";
import Main from "./components/Main/Main.jsx";
import Aside from "./components/Aside/Aside.jsx";
import Footer from "./components/Footer/Footer.jsx";
import Modal from "./components/Common/Modal/Modal";
import {
    modalIDs,
    ModalParameters
} from "./components/Common/Modal/ModalParameters.js";
import Button from "./components/Common/Button/Button.jsx";
import {createDeepCopy} from "./utilities/createDeepCopy/createDeepCopy.js";

class App extends React.Component {
    state = {
        openModalID: "",
        chosenGoodsCardID: "",
        goods: "" || JSON.parse(localStorage.getItem("goods"))
    };

    render() {
        const {goods} = this.state;

        this.putDataInLocalStorage();

        const chosenModal = this.getChosenModal();

        return (
            <React.Fragment>
                <Header/>
                <div className="wrapper main">
                    <Aside/>
                    <Main
                        modalIDs={modalIDs}
                        goods={goods}
                        toggleFavourite={id => this.toggleFavourite(id)}
                        toggleCartModal={e => this.toggleCartModal(e)}
                        toggleModalClosing={e => this.toggleModalClosing(e)}/>
                </div>
                <Footer/>
                {chosenModal && <Modal {...chosenModal} />}
            </React.Fragment>
        );
    }

    async componentDidMount() {
        const localStorageData = JSON.parse(localStorage.getItem("goods"));

        if (localStorageData) {
            this.setState({
                goods: localStorageData
            });
            return;
        }

        const response = await fetch("./goods.json");
        const data = await response.json();
        if (!data) {
            this.setState({
                goods: ""
            });
            return;
        }

        this.setState({
            goods: data
        });
    }

    putDataInLocalStorage() {
        const {goods} = this.state;

        localStorage.setItem("goods", JSON.stringify(goods));
    }

    toggleModalClosing(event) {
        const {openModalID} = this.state;
        const modalID = this.findDataSetName(event,
            "modalId",
            "[data-modal-id]");

        if (openModalID === modalID) {
            this.setState({openModalID: ""});
            return;
        }

        this.setState({openModalID: modalID});
    }

    toggleCartModal(event) {

        const chosenModalID = this.findDataSetName(event,
            "modalId",
            "[data-modal-id]");
        const chosenCardId = this.findDataSetName(event,
            "cardId",
            "[data-card-id]");

        const chosenModal = ModalParameters.find(modal =>
            modal.modalID.toString() === chosenModalID.toString());

        if (!chosenModal) {
            return null;
        }

        const submitBtn = chosenModal.actions.find(btn => btn.name === "submit-btn");

        submitBtn.onClick = (e) => {
            this.toggleCart(chosenCardId);
        };

        this.toggleModalClosing(event);
    }

    toggleFavourite(chosenCardID) {
        this.toggleGoodsTab(chosenCardID, "favourites");
    }

    toggleCart(chosenCardID) {
        this.toggleGoodsTab(chosenCardID, "cart");
    }

    toggleGoodsTab(chosenCardID, tabName) {
        const {goods} = this.state;
        let {allGoods} = goods;
        let tab = goods[tabName];

        const chosenItem = allGoods.find(item => item.id.toString() === chosenCardID.toString());
        if (!chosenItem) return null;

        let itemInTab;
        if (tab.length !== 0) {
            itemInTab = tab.find(item => item.id === chosenCardID);
        }

        if (itemInTab) {
            tab = tab.filter(item => item.id !== chosenCardID);
        } else {
            tab.push(chosenItem);
        }

        this.setState({
            goods: {
                ...goods,
                [tabName]: tab,
            }
        });
    }

    findDataSetName(event, datasetName, datasetSelector) {
        datasetName = datasetName.toString()
        datasetSelector = datasetSelector.toString()
        const target = event.target;
        const targetChildren = [...target.children];
        const childContainsDataset = targetChildren.find(child =>
            child.dataset[datasetName]);
        const parentContainsDataset = target.closest(datasetSelector);
        const elementContainsDataset = childContainsDataset ||
            parentContainsDataset;

        return elementContainsDataset.dataset[datasetName];
    }

    getChosenModal() {
        const {openModalID} = this.state;

        if (!openModalID) {
            return null;
        }

        /** This method is used to make a deep copy. Because there are
         *  Arrays in there that don't change their place in memory if
         *  shallow copy is used. For instance : [..spreadOperator],
         *  slice(), Object.assign() etc */
        const ModalParametersCopy = createDeepCopy(ModalParameters);


        let chosenModal = ModalParametersCopy.find(modal => {
            return modal.modalID.toString() === openModalID.toString();
        });

        if (!chosenModal) {
            return null;
        }

        chosenModal.actions = chosenModal.actions.map(btn => {
            return <Button {...btn}/>;
        });

        chosenModal.toggleModalClosing = (e) => this.toggleModalClosing(e);

        return chosenModal;
    }

}

export default App;
