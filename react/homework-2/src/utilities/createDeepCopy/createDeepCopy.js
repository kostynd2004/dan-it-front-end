export function createDeepCopy(inputObject){
    let outputObject, key, value;

    if (typeof inputObject !== "object" || inputObject === null){
        return inputObject
    }

    outputObject = Array.isArray(inputObject) ? [] : {}

    for (key in inputObject) {
        value = inputObject[key]

        // Recursively (deep) copy for nested objects, including arrays
        outputObject[key] = createDeepCopy(value)
    }

    return outputObject
}