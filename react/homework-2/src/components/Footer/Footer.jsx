import React, {Component} from "react";
import "./Footer.scss"

class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <p className="footer__copyright">
                    Copyright © 2021  | All Rights Reserved
                </p>
            </footer>
        );
    }
}

export default Footer;