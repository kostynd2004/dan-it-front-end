import React, {Component} from "react";
import Button from "../../../Common/Button/Button.jsx";
import {
    starIcon,
    emptyStarIcon,
    cartGoodsIcon,
    cartChecked
} from "../../../../utilities/Icons/iconDetails.js";
import "./GoodsCard.scss";
import PropTypes from "prop-types";

class GoodsCard extends Component {
    state = {
        isFavourite: false,
        isOnCart: false
    };

    render() {
        const {props, state} = this;
        const {card, modalIDs} = props;

        if (!card) {
            return null;
        }
        const {title, price, image} = card;
        const modalID = state.isOnCart ? modalIDs.deleteFromCart : modalIDs.cart;

        return (
            <li className="goods__cell">
                <div className="goods-tile">
                    <div className="goods-tile__inner">
                        <a href="#" className="goods-tile__picture">
                            <img src={image} alt={title}/>
                        </a>
                        <div className="goods-tile__actions">
                            <Button type="button"
                                    onClick={() => this.handleFavouriteBtnClick()}
                                    className="goods-tile__favourite-btn"
                                    children={state.isFavourite ?
                                        starIcon : emptyStarIcon}/>
                        </div>
                        <div className="goods-tile__colors">
                        </div>
                        <a href="#" className="goods-tile__title">{title}</a>
                        <div className="goods-tile__prices">
                            <p className="goods-tile__price">
                                <span
                                    className="goods-tile__price-value">{price.toString()}</span>
                                <span
                                    className="goods-tile__price-currency">₴</span>
                            </p>
                            <Button
                                className="goods-tile__cart-button"
                                data-modal-id={modalID}
                                data-card-id={card.id}
                                children={state.isOnCart ?
                                    cartChecked : cartGoodsIcon}
                                onClick={(e) => props.toggleCartModal(e)}/>
                        </div>
                    </div>
                </div>
            </li>
        );
    }

    componentWillReceiveProps(nextProps) {
        const {goods, card} = nextProps;

        const isOnCart = goods.cart.some(item => item.id === card.id);

        this.setState({
            isOnCart: isOnCart
        });

        const isFavourite = goods.favourites.some(item => item.id === card.id);

        this.setState({
            isFavourite
        });

    }

    handleFavouriteBtnClick() {
        const {props, state} = this;
        this.setState({
            isFavourite: !state.isFavourite
        });

        props.toggleFavourite(props.card.id);
    }

}

GoodsCard.propTypes = {
    card: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        image: PropTypes.string,
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        color: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)])
    }),
    modalIDs: PropTypes.object.isRequired,
    goods: PropTypes.shape({
        cart: PropTypes.array,
        favourites: PropTypes.array,
        allGoods: PropTypes.arrayOf(PropTypes.object)
    }),
    toggleModalClosing: PropTypes.func.isRequired,
    toggleCartModal: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired
};

export default GoodsCard;