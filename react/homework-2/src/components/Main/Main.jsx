import React, {Component} from "react";
import Goods from "./Goods/Goods.jsx";
import PropTypes from "prop-types"
import "./Main.scss"

class Main extends Component {
    render() {
        const {props} = this;

        return (
            <main className="main-content">
                <Goods
                    modalIDs={props.modalIDs}
                    goods={props.goods}
                    toggleModalClosing={props.toggleModalClosing}
                    toggleCartModal={props.toggleCartModal}
                    toggleFavourite={props.toggleFavourite}/>
            </main>
        );
    }
}

Main.propTypes = {
    modalIDs: PropTypes.object.isRequired,
    goods: PropTypes.shape({
        cart: PropTypes.array,
        favourites: PropTypes.array,
        allGoods: PropTypes.arrayOf(PropTypes.object)
    }),
    toggleModalClosing: PropTypes.func.isRequired,
    toggleCartModal: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired
}

export default Main;