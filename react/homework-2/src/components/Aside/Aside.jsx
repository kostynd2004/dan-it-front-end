import React, {Component} from "react";
import Nav from "./Nav/Nav.jsx";

class Aside extends Component {
    render() {
        return (
            <aside className="aside">
                <Nav/>
            </aside>
        );
    }
}

export default Aside;