import React, {Component} from "react";
import Button from "../Button/Button.jsx";
import PropTypes from "prop-types";
import "./Modal.scss";
import "../../../styles/common.scss";

export default class Modal extends Component {

    render() {
        const {props} = this;
        const {modalID, header, text, closeButton, actions, classNames} = props;
        const crossButton = <Button
            text="X"
            backgroundColor="transparent"
            className={classNames.crossButton}
            onClick={(e) => props.toggleModalClosing(e)}/>;

        return (
            <div onClick={(e) => this.handleModalClosing(e)}
                 className={classNames.modal}>
                <div data-modal-id={modalID} className={classNames.modalBody}>
                    <div className={classNames.headerWrapper}>
                        {closeButton && crossButton}
                        <p className={classNames.header}>{header}</p>
                    </div>
                    <p className={classNames.text}>{text}</p>
                    <div className={classNames.buttons}>
                        {actions}
                    </div>
                </div>
            </div>
        );
    }

    handleModalClosing(e) {
        const {props} = this;
        const target = e.target;
        const targetTagName = e.target.tagName.toLowerCase();

        if (target === e.currentTarget || targetTagName === "button") {
            props.toggleModalClosing(e);
        }
    }

    componentDidMount() {
        document.body.classList.add("lock-screen");
    }

    componentWillUnmount() {
        document.body.classList.remove("lock-screen");
    }

}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.arrayOf(PropTypes.element).isRequired,
    closeButton: PropTypes.bool.isRequired,
    classNames: PropTypes.shape({
        modal: PropTypes.string,
        modalBody: PropTypes.string,
        crossButton: PropTypes.string,
        header: PropTypes.string,
        headerWrapper: PropTypes.string,
        text: PropTypes.string,
        buttons: PropTypes.string
    })
};

Modal.defaultProps = {
    classNames: {
        modal: "modal",
        modalBody: "modal-body",
        crossButton: "cross-btn modal-body__cross-btn",
        header: "modal-body__header",
        headerWrapper: "modal-body__header-wrapper",
        text: "modal-body__text",
        buttons: "modal-body__buttons"
    }
};

