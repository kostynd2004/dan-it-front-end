import {uniqueKey} from "../../../utilities/numberGenerators/numberGenerators.js";

export let ModalParameters = [
    {
        modalID: 0,
        name: "delete",
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it won’t be possible to undo" +
            " this action. Are you sure you want to delete it?",
        closeButton: true,
        actions: [
            {
                key: uniqueKey(),
                text: "OK",
                backgroundColor: "#B3372BFF",
                className: "modal-body__btn modal__submit-btn"
            }, {
                key: uniqueKey(),
                text: "Cancel",
                backgroundColor: "#B3372BFF",
                className: "modal-body__btn modal__cancel-btn"
            }
        ],
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--red",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--red",
            text: "modal-body__text",
            buttons: "modal-body__buttons"
        }
    },
    {
        modalID: 1,
        name: "cart",
        header: "Do you want to add this item into the cart?",
        text: "This item will be added to your cart",
        closeButton: true,
        actions: [
            {
                name: "submit-btn",
                key: uniqueKey(),
                text: "OK",
                backgroundColor: "#23A77BFF",
                className:
                    "modal-body__btn modal__submit-btn"
            }, {
                key: uniqueKey(),
                text: "Cancel",
                backgroundColor: "#23A77BFF",
                className: "modal-body__btn modal__cancel-btn"
            }
        ],
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--teal",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--teal",
            text: "modal-body__text",
            buttons: "modal-body__buttons"
        }
    },
    {
        modalID: 2,
        name: "deleteFromCart",
        header: "Do you want to delete this item from your cart?",
        text: "This item will be deleted from your cart",
        closeButton: true,
        actions: [
            {
                name: "submit-btn",
                key: uniqueKey(),
                text: "OK",
                backgroundColor: "#23A77BFF",
                className:
                    "modal-body__btn modal__submit-btn"
            }, {
                key: uniqueKey(),
                text: "Cancel",
                backgroundColor: "#23A77BFF",
                className: "modal-body__btn modal__cancel-btn"
            }
        ],
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--teal",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--teal",
            text: "modal-body__text",
            buttons: "modal-body__buttons"
        }

    }
];


let modalIDs = {};

ModalParameters.forEach(item => {
    modalIDs[item.name] = item.modalID;
});

export {modalIDs};
