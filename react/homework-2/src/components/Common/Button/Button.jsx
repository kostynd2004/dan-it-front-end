import React, {Component} from 'react';
import PropTypes from "prop-types";

class Button extends Component {
    render() {
        const {props} = this;
        let {backgroundColor,className, onClick, text, children, ...restProps } = props;

        const styles = {
            backgroundColor: backgroundColor
        }

        return (
            <button {...restProps}
                className={ className }
                onClick={(e) => {
                    return onClick ? onClick(e) : null
                }}
                style={ styles }>
                { text }
                {children}
            </button>
        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
}

Button.defaultProps = {
    className: "btn",
    text: "",
    backgroundColor: "transparent",
    children: ""
}

export default Button;