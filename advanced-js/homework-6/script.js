class Table {
    constructor(parentSelector, numberOfRows, numbersOfColumns, levels) {
        this.levels = levels;
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numbersOfColumns;
        this.parent = document.querySelector(parentSelector);
        this.computerScore = 0;
        this.userScore = 0
    }

    elements = {
        reloadBtn: document.createElement("button"),
        playBtn: document.createElement("button"),
        table: document.createElement("table"),
        rows: () => {
            let rows = [];
            for (let i = 0; i < this.numberOfRows; i++) {
                rows.push(document.createElement("tr"));
            }
            return rows
        },
        columns: () => {
            let columns = [];
            for (let i = 0; i < this.numberOfRows * this.numberOfColumns; i++) {
                columns.push(document.createElement("td"));
            }
            return columns
        },
        popUpWrapper: document.createElement("div"),
        popUp: document.createElement("div"),
        popUpMessage: document.createElement("p"),
        popUpBtn: document.createElement("button"),
        radioContainer: document.createElement("div"),
        radioLabel: () => {
            let labels = [];
            for (let i = 0; i < this.levels; i++) {
                labels.push(document.createElement("label"));
            }
            return labels;
        },
        radio: () => {
            let checkboxes = [];
            for (let i = 0; i < this.levels; i++) {
                checkboxes.push(document.createElement("input"))
            }
            return checkboxes;
        },
        radioTitle: document.createElement("p"),
        scoresContainer: document.createElement("div"),
        scoresTitle: document.createElement("p"),
        scores: () => {
            let scores = [];
            for (let i = 0; i < 2; i++) {
                scores.push(document.createElement("p"));
            }
            return scores;
        }
    }


    play(ms, ...elements) {
        const [scoresArr, popUpMessage] = elements;
        let cells = document.querySelectorAll(".game-table__cell");
        this.highlightCells(cells, ms, scoresArr, popUpMessage)
    }

    showResultPopUp(element, flag){
        const {popUpWrapper} = this.elements;
        this.parent.append(popUpWrapper)
        if(flag){
            element.innerHTML = `Congratulations! <br> You Won!`
            return;
        }
        element.innerHTML = `You Lost :( <br> Try again`

    }

    highlightCells = (cellsNodeList, timer, ...elements) => {
        let showMessage = this.showResultPopUp.bind(this)
        let cells = [...cellsNodeList];
        let computerScore = this.computerScore;
        const [scoresArr, popUpMessage] = elements;

        function getRandomInt(numberOfRows, numberOfColumns) {
            let min = 1;
            let max = numberOfRows * numberOfColumns
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        let {numberOfRows, numberOfColumns} = this;
        let timerId = setInterval(function a() {
            let randomNumber = getRandomInt(numberOfRows, numberOfColumns);
            let halfOfTable = Math.ceil((numberOfColumns * numberOfRows) / 2);
            let missedCell = cells.find(item => item.classList.contains("active"))
            if (missedCell) {
                missedCell.classList.remove("active");
                missedCell.classList.add("missed");
                computerScore += 1;
                scoresArr[0].textContent = `Computer - ${computerScore}`;
            }

            let clickedCells = document.querySelectorAll(".game-table__cell.clicked")
            let missedCells = document.querySelectorAll(".game-table__cell.missed")

            if (clickedCells.length === halfOfTable) {
                showMessage(popUpMessage, true);
                clearInterval(timerId);
                return
            } else if (missedCells.length === halfOfTable) {
                showMessage(popUpMessage, false)
                clearInterval(timerId);
                return;
            }

            if (cells[randomNumber - 1].classList.contains("active") || cells[randomNumber - 1].classList.contains("clicked") || cells[randomNumber - 1].classList.contains("missed")) {
                a();
            } else {
                cells[randomNumber - 1].classList.add("active");
            }
        }, timer)
    }

    _counter() {
        let counter = 0;
        return function () {
            return counter++
        }
    }

    addEventListeners(...elements) {
        const {reloadBtn,playBtn,table, rows, columns, popUp, popUpMessage, popUpBtn, radioContainer, popUpWrapper} = this.elements;
        const [radioButtonsArr, scoresArr] = elements;
        let checkedRadio = radioButtonsArr.find(item => item.checked === true);
        let timer;
        if (checkedRadio.value === "Easy") {
            timer = 1500;
        } else if (checkedRadio.value === "Medium") {
            timer = 1000;
        } else if (checkedRadio.value === "Hard") {
            timer = 500;
        }

        radioContainer.addEventListener("click", event => {
            if (event.target.checked === true) {
                if (event.target.value === "Easy") {
                    timer = 1500;
                } else if (event.target.value === "Medium") {
                    timer = 1000;
                } else if (event.target.value === "Hard") {
                    timer = 500;
                }
            }
        })

        playBtn.addEventListener("click", event => {
                this.play(timer, scoresArr, popUpMessage);
        })

        reloadBtn.addEventListener("click", event =>{
            document.location.reload();
        })

        table.addEventListener("click", event => {
            if (event.target.classList.contains("active")) {
                event.target.classList.remove("active")
                event.target.classList.add("clicked")
                this.userScore += 1;
                scoresArr[1].textContent = `User - ${this.userScore}`;
            }
        })

        popUpWrapper.addEventListener("click", event=>{
            if (!(event.target === popUpMessage || event.target === popUp)){
                popUpWrapper.remove()
            }
        })
    }

    addText(...elements) {
        const [radioLabelArr, radioButtonsArr, radioContainer, radioTitle, scoresTitle, scoresArr, playBtn, popUpMessage, popUpBtn, reloadButton] = elements;
        const levelsTitle = ["Easy", "Medium", "Hard", "Pro", "Legend"];

        reloadButton.textContent = "Restart";
        playBtn.textContent = "Play";
        radioLabelArr.forEach((item, index) => {
            item.textContent = levelsTitle[index];
        })
        radioButtonsArr.forEach((item, index) => {
            item.value = levelsTitle[index];
            item.name = "difficulty";
        })
        radioTitle.textContent = "Choose the level: ";
        scoresTitle.textContent = "Scores: ";
        scoresArr[0].textContent  = `Computer - ${this.computerScore}`;
        scoresArr[1].textContent = `User - ${this.userScore}`;
        popUpBtn.textContent = "X";
    }

    addStyles(columnsArr, ...elements) {
        const {
            reloadBtn,
            playBtn,
            table,
            popUpWrapper,
            popUp,
            popUpMessage,
            popUpBtn,
            radioContainer,
            radioTitle,
            scoresContainer,
            scoresTitle
        } = this.elements;
        const [radioLabelArr, radioButtonsArr, scoresArr] = elements;

        reloadBtn.classList.add("game__button");
        playBtn.classList.add("game__button");
        radioTitle.classList.add("game-levels__title");
        radioContainer.classList.add("game-levels");
        radioButtonsArr.forEach(item => item.classList.add("game-levels__radio"));
        radioLabelArr.forEach(item => item.classList.add("game-levels__label"));
        scoresContainer.classList.add("game-scores");
        scoresTitle.classList.add("game-scores__title");
        scoresArr.forEach(item => item.classList.add("game-scores__text"));
        popUpWrapper.classList.add("pop-up-wrapper");
        popUp.classList.add("pop-up");
        popUpBtn.classList.add("pop-up__button");
        popUpMessage.classList.add("pop-up__text");

        table.classList.add("game-table");
        columnsArr.forEach(item => {
            item.classList.add("game-table__cell");
        })
    }

    render() {
        const {
            reloadBtn,
            playBtn,
            table,
            rows,
            columns,
            radioLabel,
            radioContainer,
            radio,
            radioTitle,
            scoresTitle,
            scoresContainer,
            scores,
            popUpWrapper,
            popUp,
            popUpMessage,
            popUpBtn
        } = this.elements;
        const columnsArr = columns();
        const rowsArr = rows();
        const radioLabelArr = radioLabel();
        const radioButtonsArr = radio();
        const scoresArr = scores();

        let counter = this._counter()

        radioButtonsArr.forEach(item => item.type = "radio")
        radioButtonsArr[0].checked = true

        this.addStyles(columnsArr, radioLabelArr, radioButtonsArr, scoresArr);
        this.addText(radioLabelArr, radioButtonsArr, radioContainer, radioTitle, scoresTitle, scoresArr, playBtn, popUpMessage, popUpBtn, reloadBtn);
        this.addEventListeners(radioButtonsArr, scoresArr);

        this.parent.append(scoresContainer,playBtn,reloadBtn,table, radioContainer);

        rowsArr.forEach(item => {
            table.append(item);
            for (let i = 0; i < this.numberOfColumns; i++) {
                item.append(columnsArr[counter()]);
            }
        });

        radioContainer.prepend(radioTitle)
        radioLabelArr.forEach((item, index) => {
            radioContainer.append(item);
            item.append(radioButtonsArr[index]);
        });

        scoresContainer.append(scoresTitle);
        scoresArr.forEach(item => scoresContainer.append(item))

        popUpWrapper.append(popUp);
        popUp.append(popUpBtn, popUpMessage)
    }


}

function responsiveDesign(){
    if(window.outerWidth >= 320 && window.outerWidth < 768) {
        const table = new Table("#container", 5, 5, 3);
        table.render()
    }else if (window.outerWidth >= 768 && window.outerWidth < 1024){
        const table = new Table("#container", 6, 6, 3);
        table.render()
    }else if (window.outerWidth >= 1024 && window.outerWidth < 1240){
        const table = new Table("#container", 7, 7, 3);
        table.render()
    }else if(window.outerWidth >= 1024){
        const table = new Table("#container", 10, 10, 3);
        table.render()
    }

}

responsiveDesign()
